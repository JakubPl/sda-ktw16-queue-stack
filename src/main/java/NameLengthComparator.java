import java.util.Comparator;

public class NameLengthComparator implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {

        return o2.length() - o1.length();

        /*int o1Length = o1.length();
        int o2Length = o2.length();
        if(o1Length > o2Length) {
            return -1;
        } else if(o1Length < o2Length) {
            return 1;
        } else {
            return 0;
        }*/
    }
}
