import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        System.out.println("Kolejka zwykla");
        Queue<String> queueOfNames = new LinkedList<String>();
        queueOfNames.add("Tomek");
        queueOfNames.add("Paweł");
        queueOfNames.add("Kasia");
        queueOfNames.add("Ola");

        System.out.println(queueOfNames.peek());
        System.out.println(queueOfNames.remove());
        System.out.println(queueOfNames.remove());
        System.out.println(queueOfNames.remove());
        System.out.println(queueOfNames.remove());
        System.out.println(queueOfNames.poll());
        System.out.println("Stos");
        Stack<String> stackOfNames = new Stack<String>();

        stackOfNames.add("Tomek");
        stackOfNames.add("Paweł");
        stackOfNames.add("Kasia");
        stackOfNames.add("Ola");

        System.out.println(stackOfNames.pop());
        System.out.println(stackOfNames.pop());
        System.out.println(stackOfNames.pop());
        System.out.println(stackOfNames.pop());
        System.out.println(stackOfNames.firstElement());

        System.out.println("Kolejka priorytetowa: ");

        PriorityQueue<String> priorityQueue = new PriorityQueue<>(new NameLengthComparator());

        priorityQueue.add("Tomek1111");
        priorityQueue.add("Paweł11");
        priorityQueue.add("Kasia");
        priorityQueue.add("Ola");

        System.out.println(priorityQueue.remove());
        System.out.println(priorityQueue.remove());
        System.out.println(priorityQueue.remove());
        System.out.println(priorityQueue.remove());



    }
}
